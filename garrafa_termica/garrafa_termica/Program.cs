using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Fonte para as fórmulas e constantes: http://labvirtual.eq.uc.pt/siteJoomla/index.php?option=com_content&task=view&id=248&Itemid=

namespace garrafa_termica {

    class Program {

        static double diametroGrande = 0.072; //Diâmetro exterior da garrafa de vidro interna 7.2cm ou 0.072m (considerando 2mm de espessura do vidro 2mm)
        static double diametroPequeno = 0.07; //Diâmetro interno da garrafa de vidro interna 7cm ou 0.07m
        static double alturaGarrafa = 0.20; //Altura da garrafa = 20cm ou 0.2m
        static double emissividadeAluminio = 0.06; //Emissividade do alumínio polido
        static double constanteStefanBoltzmann = 5.67 * (Math.Pow(10, -8));
        static double coeficienteTransfCalor = 50; //Convecção natural em líquidos, sem movimento (W.m^(-2).K^(-1))
        static double calorEspecificoLiquido = 15.328; //Calor específico da água em (J/kg.K)

        static double tempInicial; //Em KELVIN
        static double tempFinal; //Em KELVIN
        static double constanteLiquido;
        static double constanteVidro;        

        static double areaAluminio = 2 * Math.PI * (diametroGrande / 2) * alturaGarrafa;

        /*
         * 3,15 graus celsius = -270K
         * 18,15 graus Celsius = -255K
         * 
         */

        static void printCollectedData() {
            Console.WriteLine("\n-----------------------------------------------");
            Console.WriteLine("DADOS INFORMADOS: ");
            Console.WriteLine("Temperatura inicial informada: " + tempInicial);
            Console.WriteLine("Temperatura final informada: " + tempFinal);
            Console.WriteLine("Constante do líquido informada: " + constanteLiquido);
            Console.WriteLine("Constante do material informada:" + constanteVidro);
            Console.WriteLine("-----------------------------------------------");
            Console.WriteLine("Aguarde. Calculando...");
            Console.WriteLine("-----------------------------------------------\n");
        }

        static double calcularConducao() {
            return 2 * Math.PI * constanteVidro * alturaGarrafa * ((tempFinal - tempInicial) / (Math.Log((diametroGrande / 2) / (diametroPequeno / 2)))); 
        }

        static double calcularRadiacao() {
            return constanteStefanBoltzmann * emissividadeAluminio * areaAluminio * (Math.Pow(tempFinal, 4) - Math.Pow(tempInicial, 4));
        }

        static double calcularConveccao() {
            return coeficienteTransfCalor * areaAluminio * (tempFinal - tempInicial);
        }

        static double calcularTotalCalor(double resultConducao, double resultRadiacao, double resultConveccao) {
            return resultConducao + resultRadiacao + resultConveccao;
        }
        /*
        static double calcularTempFinal(double quantidadeCalor) {

            return (((-1) * quantidadeCalor) / (0.7 * calorEspecificoLiquido)) + tempInicial;
        }
        */
        static void mostrarResultados(double resultConducao, double resultRadiacao, double resultConveccao, double resultTotal) {
            
            Console.Clear();
            Console.WriteLine("RESULTADOS OBTIDOS: \n");
            Console.WriteLine("Após a condução: " + resultConducao);
            Console.WriteLine("Após a radiação: " + resultRadiacao);
            Console.WriteLine("Após a Convecção: " + resultConveccao);
            if (resultTotal > 0) {
                Console.WriteLine("A QUANTIDADE TOTAL DE CALOR ADQUIRIDO FOI: " + Math.Abs(resultTotal));
            } else if (resultTotal < 0) {
                Console.WriteLine("A QUANTIDADE TOTAL DE CALOR PERDIDO FOI: " + Math.Abs(resultTotal));
            } else {
                Console.WriteLine("NÃO HOUVE TRANSFERÊNCIA TÉRMICA NO RECIPIENTE.");
            }

            //Console.WriteLine("TEMPERATURA FINAL DA ÁGUA: " + calcularTempFinal(resultTotal));
        }

        static void Main(string[] args) {

            //Frase de boas-vindas
            Console.WriteLine("Bem-vindo ao simulador do FRASCO DE DEWAR!");

            //Solicitando a temperatura inicial ao usuário
            Console.WriteLine("Informe a temperatura inicial (Celsius): ");
            tempInicial = Double.Parse(Console.ReadLine()) + 273.15;

            //Solicitando a temperatura final ao usuário
            Console.WriteLine("Informe a temperatura final (Celsius): ");
            tempFinal = Double.Parse(Console.ReadLine()) + 273.15;

            //Solicitando a constante do líquido ao usuário
            Console.WriteLine("Informe a constante do líquido: ");
            constanteLiquido = Double.Parse(Console.ReadLine(3);

            //Solicitando a constante do material ao usuário
            Console.WriteLine("Informe a constante do material: ");
            constanteVidro = Double.Parse(Console.ReadLine());

            //Mostrando o resultado para o usuário confirmar as informações que digitou
            printCollectedData();

            Thread.Sleep(3000);

            //Calculando o resultado da condução e armazenando
            double resultConducao = calcularConducao();
            //Calculando o resultado da radiação e armazenando
            double resultRadiacao = calcularRadiacao();
            //Calculando o resultado da convecção e armazenando
            double resultConveccao = calcularConveccao();

            //Calculando o resultado de todas as transferências
            double quantidadeCalor = calcularTotalCalor(resultConducao, resultRadiacao, resultConveccao);

            //Mostrando ao usuário o resultado final do programa
            mostrarResultados(resultConducao, resultRadiacao, resultConveccao, quantidadeCalor);

            Console.ReadKey();


        }
    }
}

